<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App + jQuery">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <title>HappyMeter - Admin Panel</title>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <!-- =============== VENDOR STYLES ===============-->
        <!-- FONT AWESOME-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/fontawesome/css/font-awesome.min.css">
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
        <!-- ANIMATE.CSS-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/animate.css/animate.min.css">
        <!-- WHIRL (spinners)-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/whirl/dist/whirl.css">

        <!-- DATATABLES-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/datatables-colvis/css/dataTables.colVis.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/datatables/media/css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/dataTables.fontAwesome/index.css">

        <!-- =============== PAGE VENDOR STYLES ===============-->
        <!-- WEATHER ICONS-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/weather-icons/css/weather-icons.min.css">
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.css" id="maincss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom.css">

        <link href="<?php echo base_url(); ?>assets/css/toastr.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ion.rangeSlider.css">
        
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ion.rangeSlider.skinFlat.css">

        <!-- JQUERY-->
        <script src="<?php echo base_url() ?>assets/vendor/jquery/dist/jquery.js"></script>   


    </head>
    <body>
        <div class="wrapper">
            <!-- top navbar-->
            <header class="topnavbar-wrapper">
                <!-- START Top Navbar-->
                <nav role="navigation" class="navbar topnavbar">
                    <!-- START navbar header-->
                    <div class="navbar-header">
                        <a href="<?php echo base_url() ?>dashboard" class="navbar-brand">
                            <div class="brand-logo">
                                <img src="<?php echo base_url() ?>assets/img/logo.png" alt="App Logo" class="img-responsive">
                            </div>
                            <div class="brand-logo-collapsed">
                                <img src="<?php echo base_url() ?>assets/img/logo-single.png" alt="App Logo" class="img-responsive">
                            </div>
                        </a>
                    </div>
                    <!-- END navbar header-->
                    <!-- START Nav wrapper-->
                    <div class="nav-wrapper">
                        <!-- START Left navbar-->
                        <ul class="nav navbar-nav">
                            <li>
                                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                                <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                                    <em class="fa fa-navicon"></em>
                                </a>
                                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                                    <em class="fa fa-navicon"></em>
                                </a>
                            </li>
                        </ul>
                        <!-- END Left navbar-->
                        <!-- START Right Navbar-->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- START user name-->
                            <li class="dropdown dropdown-list user-name">
                                <a href="#" data-toggle="dropdown">
                                    <em class="icon-user"></em> Admin
                                </a>
                                <!-- START Dropdown menu-->
                                <ul class="dropdown-menu animated flipInX">
                                    <li>
                                        <!-- START list group-->
                                        <div class="list-group">                                          
                                            <a href="<?php echo base_url() ?>Login/logout" class="list-group-item">
                                                <small>Logout</small>
                                            </a>
                                        </div>
                                        <!-- END list group-->
                                    </li>
                                </ul>
                                <!-- END Dropdown menu-->
                            </li>
                            <!-- END Alert menu-->
                        </ul>
                        <!-- END Right Navbar-->
                    </div>
                    <!-- END Nav wrapper-->
                </nav>
                <!-- END Top Navbar-->
            </header>