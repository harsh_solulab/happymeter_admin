<!-- Page footer-->
<footer>
    <span>&copy; 2018 - Happymeter</span>
</footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="<?php echo base_url() ?>assets/vendor/modernizr/modernizr.custom.js"></script>
<!-- MATCHMEDIA POLYFILL-->
<script src="<?php echo base_url() ?>assets/vendor/matchMedia/matchMedia.js"></script>

<!-- BOOTSTRAP-->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<!-- STORAGE API-->
<script src="<?php echo base_url() ?>assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
<!-- JQUERY EASING-->
<script src="<?php echo base_url() ?>assets/vendor/jquery.easing/js/jquery.easing.js"></script>
<!-- ANIMO-->
<script src="<?php echo base_url() ?>assets/vendor/animo.js/animo.js"></script>
<!-- SLIMSCROLL-->
<script src="<?php echo base_url() ?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<!-- SCREENFULL-->
<script src="<?php echo base_url() ?>assets/vendor/screenfull/dist/screenfull.js"></script>
<!-- LOCALIZE-->
<script src="<?php echo base_url() ?>assets/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>

<script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/ion.rangeSlider.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>


<!-- DATATABLES-->
<script src="<?php echo base_url() ?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-colvis/js/dataTables.colVis.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables/media/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/dataTables.buttons.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/buttons.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/buttons.colVis.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/buttons.flash.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/buttons.html5.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-buttons/js/buttons.print.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-responsive/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/datatables-responsive/js/responsive.bootstrap.js"></script>            
</body>
</html>