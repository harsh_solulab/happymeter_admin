<!-- sidebar-->
<aside class="aside">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">
            <!-- START sidebar nav-->
            <ul class="nav">

                <!-- Iterates over all sidebar items-->
                <li class="active">
                    <a href="<?php echo base_url() ?>dashboard" title="Dashboard">
                        <em class="icon-speedometer"></em>
                        <span>Dashboard</span>
                    </a>

                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>user" title="User Management">
                        <em class="icon-people"></em>
                        <span>User Management</span>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>invitation" title="Invitation">
                        <em class="icon-envelope-letter"></em>
                        <span>Invitation</span>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>Categories" title="Category Management">
                        <em class="icon-layers"></em>
                        <span>Category Management</span>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>Subcategories" title="Sub Category Management">
                        <em class="icon-layers"></em>
                        <span>Sub Category Management</span>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>Questions" title="Question Management">
                        <em class="icon-question"></em>
                        <span>Question Management</span>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url() ?>Survey" title="Survey">
                        <em class="icon-book-open"></em>
                        <span>Survey</span>
                    </a>
                </li>
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>