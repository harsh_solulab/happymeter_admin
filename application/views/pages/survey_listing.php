<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Manage Survey
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>dashboard'">Back</button>
        </h3>
        <div class="container-fluid">
            <!-- START DATATABLE 1-->
            <div class="row">                
                <div class="col-lg-12">
                    <h4 class="mb-20">Survey Report</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">                                                        
                            <div class="table-responsive">
                                <table id="surveyReport" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Survey Taken Date</th>
                                            <th>User</th>
                                            <th>Total Question</th>
                                            <th>Answered Questions</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($userData)) {
                                            $count = 1;
                                            ?>
                                            <?php foreach ($userData as $ud) { ?>
                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php foreach ($userSurveyData as $key => $usd) { ?><?php if ($ud['_id'] == $usd['userId']) {
                                                echo date_format(new DateTime(date(DATE_ISO8601, $usd['createdAt']->sec)), "m/d/Y H:i:s");
                                            } else {
                                                echo "";
                                            } ?><?php } ?></td>
                                                    <td><?php echo $ud['fullName']; ?></td>
                                                    <td><?php echo $questionData; ?></td>
                                                <?php $ansCount = 0;
                                                foreach ($surveyResponseData as $key => $srd) { ?><?php if ($ud['_id'] == $srd['userId']) {
                                            $ansCount++;
                                        } ?><?php } ?>
                                                    <td><?php echo $ansCount; ?></td>
                                                    <td><a href="<?php echo base_url() ?>Survey/view_answer/<?php echo $ud['_id']; ?>">View Answers</a> | <a href="<?php echo base_url() ?>Survey/view_report/<?php echo $ud['_id']; ?>">View Report</a></td>
                                                </tr>
                                            <?php
                                            $count++;
                                        }
                                        ?>
                                        <?php } else { ?>
                                            <tr>No Data Found</tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- START DATATABLE 2-->
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="mb-20">No. of Survey Taken by User</h4>
                    <div class="panel panel-default">                        
                        <div class="panel-body">                            
                            <div class="table-responsive">
                                <table id="surveyTakenByUser" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>User</th>
                                            <th>Email</th>
                                            <th>Total Survey Taken</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($userData)) {
                                        $count = 1;
                                        ?>
                                            <?php foreach ($userData as $ud) { ?>
                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php echo $ud['fullName']; ?></td>
                                                    <td><?php echo $ud['email']; ?></td>
                                                    <td><?php foreach ($userSurveyData as $key => $usd) { ?><?php if ($ud['_id'] == $usd['userId']) {
                                                echo COUNT($usd['userId']);
                                            } else {
                                                echo "0";
                                            } ?><?php } ?></td>
                                                </tr>
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <tr>No Data Found</tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(function () {

        /*-----------------surveyReport-----------------*/
        var SurveyReportt = $('#surveyReport').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search Survey:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    SurveyReportt.fnFilter(this.value, columnInputs.index(this));
                });

        var SurveyTakenByUser = $('#surveyTakenByUser').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search Survey:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'csv',
                    className: 'btn-sm'
                }, {
                    extend: 'excel',
                    className: 'btn-sm',
                    title: 'XLS-File'
                }, {
                    extend: 'pdf',
                    className: 'btn-sm',
                    title: $('title').text()
                }, {
                    extend: 'print',
                    className: 'btn-sm'
                }]
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    SurveyTakenByUser.fnFilter(this.value, columnInputs.index(this));
                });
    });

</script>