<!-- Main section-->
<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Update Sub Category
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>Subcategories'">Back</button>
        </h3>
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" id="editSubCat" action="<?php echo base_url() ?>Subcategories/editSubCat" class="form-horizontal mt-20">
                    <?php if (!empty($catData)) { ?>
                        <?php foreach ($catData as $scd) { ?>
                        <input type="hidden" name="id" value="<?php echo $scd['_id']; ?>">
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Survey</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="survey" id="survey" class="form-control m-b">
                                                <option value="">Select Survey</option>
                                                <?php if ($surveyData) { ?>
                                                    <?php foreach ($surveyData as $sd) { ?>
                                                        <option value="<?php echo $sd['_id']; ?>" <?php if ($scd['surveyId'] == $sd['_id']) { echo "selected"; } ?>><?php echo $sd['title']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>                                
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Category</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="cat" id="cat" class="form-control m-b">
                                                <option value="">Select Associated Category</option>
                                                <?php if ($categoriesData) { ?>
                                                    <?php foreach ($categoriesData as $cd) { ?>
                                                        <option value="<?php echo $cd['_id']; ?>" <?php if ($scd['parentId'] == $cd['_id']) { echo "selected"; } ?>><?php echo $cd['title']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Sub Category</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="subcat" id="subcat" value="<?php echo $scd['title']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>                
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-4">                                 
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" onclick="location.href = '<?php echo base_url() ?>/Subcategories'">Cancel</button>
                                </div>
                            </div>                    
                        <?php } ?>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {
        $("#editSubCat").validate({
            rules: {
                survey: {
                    required: true
                },
                subcat: {
                    required: true
                }
            }
        });

    });

</script>