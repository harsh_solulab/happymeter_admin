<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>
            User Details
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>user'">Back</button>
        </h3>
        <div class="container-fluid">                 
            <div class="row">
                <div class="col-lg-12">
                    <!-- User Details -->
                    <?php if (!empty($userData)) { ?>
                        <?php foreach ($userData as $ud) { ?>
                            <div class="well clearfix">
                                <h4 class="col-xs-12 mb-20"><strong>Full Name :</strong> <?php echo $ud['fullName']; ?></h4>
                                <div class="col-sm-6 col-xs-12">
                                    <p><strong>Gender :</strong> <?php echo isset($ud['gender']) ? $ud['gender'] : "-"; ?></p>
                                    <p><strong>Email :</strong> <?php echo $ud['email']; ?></p>
                                    <p><strong>Total Survey Taken :</strong> <?php echo $userSurveyData; ?></p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <p><strong>Member Since :</strong> <?php echo date_format(new DateTime(date(DATE_ISO8601, $ud['createdAt']->sec)), "Y/m/d H:i:s"); ?></p>
                                    <p><strong>Status :</strong> <span class="text-success"><?php echo ($ud['isActive'] == "1") ? "Active" : "In-Active"; ?></span></p>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>