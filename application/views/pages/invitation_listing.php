<!-- Main section-->
<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Invitation Management
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>dashboard'">Back</button>
        </h3>
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="notifications"><?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <?php if ($this->session->flashdata('danger')) { ?>
                <div class="notification"><?php echo $this->session->flashdata('danger'); ?></div>
            <?php } ?>
            <!-- START DATATABLE 1-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 mb-20">
                                    <button type="button" class="btn btn-labeled btn-success pull-right" onclick="location.href = '<?php echo base_url() ?>/invitation/addInvitation'">
                                        <span class="btn-label"><i class="fa fa-plus"></i>
                                        </span>Add New</button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="invitationManagement" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Email</th>
                                            <th>Date/Time</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($invitationData)) {
                                            $count = 1; ?>
                                            <?php foreach ($invitationData as $invi) { ?>
                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php echo $invi['email']; ?></td>
                                                    <td><?php echo date_format(new DateTime($invi['createdAt']), "Y/m/d H:i:s"); ?></td>
                                                    <td><?php echo $invi['status']; ?></td>
                                                    <td><a href="javascript:;" data-id="<?php echo $invi['email']; ?>" class="btn btn-primary btn-xs inv-res">resend</a><a href="javascript:;" data-id="<?php echo $invi['_id']; ?>" class="btn btn-danger btn-xs inv-del">delete</a></td>
                                                </tr>
                                                <?php $count++;
                                            } ?>
                                        <?php } else { ?>
                                            <tr>No Data Found</tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {

        /*-----------------invitationManagement-----------------*/
        var InvitationManagement = $('#invitationManagement').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search Invitation:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    InvitationManagement.fnFilter(this.value, columnInputs.index(this));
                });


        $('.inv-res').click(function () {            
            var email = $(this).attr('data-id');           
            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>invitation/resendInvitation',
                data: {email: email},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data) {                                                         
                         location.reload();    
                        return false;
                    } else {                           
                        location.reload();
                        return false;
                    }
                }
            });

        });
        
        $('.inv-del').click(function () {            
            var id = $(this).attr('data-id');           
            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>invitation/delInvitation',
                data: {id: id},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data) {                                                         
                         location.reload();    
                        return false;
                    } else {                           
                        location.reload();
                        return false;
                    }
                }
            });

        });
    });

</script>