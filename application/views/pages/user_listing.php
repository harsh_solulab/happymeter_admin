<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Users Management
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>dashboard'">Back</button>
        </h3>
        <div class="container-fluid">          
            <!-- START DATATABLE 1-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive user-management">
                                <table id="userManagement" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Full Name</th>                                            
                                            <th>Email</th>
                                            <th>Member Since</th>                                            
                                            <th>Profile</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($userData)) {
                                            $count = 1;
                                            ?>
                                            <?php foreach ($userData as $ud) { ?>
                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php echo $ud['fullName']; ?></td>
                                                    <td><?php echo $ud['email']; ?></td>
                                                    <td><?php echo date_format(new DateTime(date(DATE_ISO8601, $ud['createdAt']->sec)), "Y/m/d H:i:s"); ?></td>                                                    
                                                    <td><?php echo ($ud['isProfileComplete'] == "1") ? "Completed" : "Remaining"; ?></td>
                                                    <td>
                                                        <label class="switch">
                                                            <?php
                                                            $status = "";
                                                            if ($ud['isActive'] == "1") {
                                                                $status = "checked";
                                                            }
                                                            ?>
                                                            <input name="status" class="status" data-id="<?php echo $ud['_id']; ?>" type="checkbox" <?php echo $status; ?>>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td><a href="<?php echo base_url() ?>user/user_details/<?php echo $ud['_id'] ?>">View Details</a></td>
                                                </tr>  
                                                <?php
                                                $count++;
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <tr>No Data Found</tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(function () {

        /*-----------------User Management-----------------*/
        var UserManagement = $('#userManagement').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search User:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },

        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    UserManagement.fnFilter(this.value, columnInputs.index(this));
                });

        $('input[name="status"]').click(function () {
            var cval = $(this).is(':checked');
            var userid = $(this).attr('data-id');
            var status = "";

            if (cval == true) {
                status = "1";
            } else {
                status = "0";
            }

            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>user/updUserStatus',
                data: {id: userid, status: status},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data) {                        
                    <?php if ($this->session->flashdata('success')) { ?>
                            Command: toastr["success"]("<?php echo $this->session->flashdata('success'); ?>")
                    <?php } ?>
                        return false;
                    } else {
                    <?php if ($this->session->flashdata('danger')) { ?>
                            Command: toastr["success"]("<?php echo $this->session->flashdata('danger'); ?>")
                    <?php } ?>
                        return false;
                    }
                }
            });

        });

    });

</script>