<html>
    <head>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.css" id="maincss">
    </head>
    <body>
        <div class="wrapper">
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="notifications"><?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <?php if ($this->session->flashdata('danger')) { ?>
                <div class="notification"><?php echo $this->session->flashdata('danger'); ?></div>
            <?php } ?>
            <div class="block-center mt-xl wd-xl">
                <!-- START panel-->
                <div class="panel panel-dark panel-flat">                   
                    <div class="panel-body">
                        <p class="text-center pv">SIGN IN TO CONTINUE.</p>
                        <form method="POST" action="<?php echo base_url() ?>Login/chklogin" class="form-horizontal mt-20">
                            <div class="form-group has-feedback">
                                <input id="username" name="username" type="text" placeholder="Enter email" autocomplete="off" required class="form-control">
                                <span class="fa fa-envelope form-control-feedback text-muted"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input id="password" name="password" type="password" placeholder="Password" required class="form-control">
                                <span class="fa fa-lock form-control-feedback text-muted"></span>
                            </div>                         
                            <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
                        </form>
                    </div>
                </div>
                <!-- END panel-->
                <div class="p-lg text-center">
                    <span>&copy;</span>
                    <span>2018</span>
                    <span>-</span>
                    <span>Happymeter</span>
                </div>
            </div>
        </div>   
    </body>
</html>