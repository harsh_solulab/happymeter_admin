<!-- Main section-->
<section>
    <!-- Page content-->
    <div class="content-wrapper dashboard-cat-list">
        <div class="content-heading">

            <!-- END Language list-->
            Dashboard
            <small>Welcome to HappyMeter !</small>
        </div>
        <!-- START widgets box-->
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>user">
                    <div class="panel widget bg-primary">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-primary-dark pv-lg">
                                <em class="icon-people fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $userCount; ?></div>
                                <div class="text-uppercase">No. of Users</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>Survey">
                    <div class="panel widget bg-purple">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-purple-dark pv-lg">
                                <em class="icon-book-open fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $surveyCount; ?></div>
                                <div class="text-uppercase">Total Survey</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>invitation">
                    <div class="panel widget bg-green">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-green-dark pv-lg">
                                <em class="icon-envelope-letter fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $invitationCount; ?></div>
                                <div class="text-uppercase">Total Invite Send</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>Categories">
                    <div class="panel widget bg-warning">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-warning-dark pv-lg">
                                <em class="icon-layers fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $categoriesCount; ?></div>
                                <div class="text-uppercase">Categories</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>Subcategories">
                    <div class="panel widget bg-success">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-success-dark pv-lg">
                                <em class="icon-layers fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $subCategoriesCount; ?></div>
                                <div class="text-uppercase">Sub Categories</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <!-- START widget-->
                <a href="<?php echo base_url() ?>Questions">
                    <div class="panel widget bg-yellow">
                        <div class="row row-table">
                            <div class="col-xs-4 text-center bg-yellow-dark pv-lg">
                                <em class="icon-question fa-3x"></em>
                            </div>
                            <div class="col-xs-8 pv-lg">
                                <div class="h2 mt0"><?php echo $questionCount; ?></div>
                                <div class="text-uppercase">Total Questions</div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- END widgets box-->

    </div>
</section>