<!-- Main section-->
<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Add New Invitation
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>invitation'">Back</button>
        </h3>
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" id="sendInvi" action="<?php echo base_url() ?>invitation/sendInvitation" class="form-horizontal mt-20">                    
                        <div class="form-group mb">
                            <label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="email" name="email" placeholder="Enter Email" class="form-control"></div>
                                </div>
                            </div>
                        </div>                    
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">                                 
                                <button type="submit" class="btn btn-primary">Send</button>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?php echo base_url() ?>index.php/invitation'">Cancel</button>
                            </div>
                        </div>                    
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {
        $("#sendInvi").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                email: "Please enter a valid email address",
            }
        });

    });

</script>