<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>
            Survey Details
            <button type="button" class="btn btn-primary pull-right back-btn no-margin" onclick="location.href = '<?php echo base_url() ?>Survey'">Back</button>
        </h3>
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="surveyReportData" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Section</th>
                                            <th>Weight</th>
                                            <th>Sub Section</th>
                                            <th>Weight</th>
                                            <th>Question</th>
                                            <th>Weight</th>
                                            <th>Satisfaction</th>                                            
                                            <th>Satisfaction Calcu</th>
                                            <th>Emoji</th>
                                            <th>Comment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($resData)) {
                                            if (!empty($questionData)) {
                                                if (!empty($categoriesData)) {
                                                    if (!empty($subCategoriesData)) {
                                                        if (!empty($surveyPCategoryResponseData)) {
                                                            if (!empty($surveySCategoryResponseData)) {
                                                                $count = 1;
                                                                foreach ($resData as $rd) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $count; ?></td>
                                                                        <td>
                                                                            <?php
                                                                            foreach ($categoriesData as $cd) {
                                                                                if (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == "") {
                                                                                    if ($rd['categoryId'] == $cd['_id']) {
                                                                                        echo $cd['title'];
                                                                                    }
                                                                                } elseif (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == $cd['_id']) {
                                                                                    echo $cd['title'];
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php                                                                            
                                                                            foreach ($surveyPCategoryResponseData as $spcd) {
                                                                                if (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == "") {
                                                                                    if ($rd['categoryId'] == $spcd['categoryId']) {
                                                                                        echo $spcd['satisfactionLevel'];
                                                                                    }
                                                                                } elseif (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == $spcd['categoryId']) {
                                                                                    echo $spcd['satisfactionLevel'];
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            foreach ($subCategoriesData as $scd) {
                                                                                if ($rd['categoryId'] == $scd['_id']) {
                                                                                    echo $scd['title'];
                                                                                }
                                                                            }
                                                                            ?> 
                                                                        </td>
                                                                        <td>
                                                                             <?php
                                                                            foreach ($surveySCategoryResponseData as $sscd) {
                                                                                if ($rd['categoryId'] == $sscd['categoryId']) {
                                                                                    echo $sscd['satisfactionLevel'];
                                                                                }
                                                                            }
                                                                            ?> 
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            foreach ($questionData as $qd) {
                                                                                if (isset($rd['questionId']) && $rd['questionId'] == $qd['_id']) {
                                                                                    echo $qd['content'];
                                                                                }
                                                                            }
                                                                            ?> 
                                                                        </td>
                                                                        <td>
                                                                            <?php echo isset($rd['satisfactionLevel']) ? $rd['satisfactionLevel'] : 0; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                            if (isset($rd['expressions'])) {

                                                                                if ($rd['expressions'] == "emo1") {
                                                                                    echo "0.00";
                                                                                } else if ($rd['expressions'] == "emo2") {
                                                                                    echo "12.50";
                                                                                } else if ($rd['expressions'] == "emo3") {
                                                                                    echo "25.00";
                                                                                } else if ($rd['expressions'] == "emo4") {
                                                                                    echo "38.50";
                                                                                } else if ($rd['expressions'] == "emo5") {
                                                                                    echo "50.00";
                                                                                } else if ($rd['expressions'] == "emo7") {
                                                                                    echo "62.50";
                                                                                } else if ($rd['expressions'] == "emo8") {
                                                                                    echo "75.00";
                                                                                } else if ($rd['expressions'] == "emo9") {
                                                                                    echo "87.50";
                                                                                } else if ($rd['expressions'] == "emo10") {
                                                                                    echo "100.00";
                                                                                }
                                                                            }
                                                                            ?>                                                                    
                                                                        </td>
                                                                        <td><?php echo isset($rd['score']) ? round($rd['score'], 2) : 0; ?></td>
                                                                        <td><img src="<?php echo base_url() ?>assets/img/emoji/<?php if (isset($rd['expressions'])) { echo $rd['expressions'] . ".png"; } else { echo "emo0.png"; } ?>" width="35"></td>
                                                                        <td><?php echo isset($rd['comments']) ? $rd['comments'] : "No Comment."; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                    $count++;
                                                                    ?>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(function () {

        /*-----------------surveyReport-----------------*/
        var SurveyReportt = $('#surveyReportData').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    SurveyReportt.fnFilter(this.value, columnInputs.index(this));
                });
    });
</script>