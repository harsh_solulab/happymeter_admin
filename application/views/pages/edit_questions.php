<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Edit Question
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>Questions'">Back</button>
        </h3>
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="POST" id="editQues" action="<?php echo base_url() ?>Questions/updQuestion" class="form-horizontal mt-20">
                    <fieldset>
                        <?php if (!empty($quesData)) { ?>
                            <?php foreach ($quesData as $qd) { ?>
                                <div class="form-group mb">
                                    <label class="col-lg-4 control-label">Associated Survey</label>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="survey" id="survey" class="form-control m-b">
                                                    <option value="">Select Associated Survey</option>
                                                    <?php if ($surveyData) { ?>
                                                        <?php foreach ($surveyData as $sd) { ?>
                                                            <option value="<?php echo $sd['_id']; ?>"  <?php if ($qd['surveyId'] == $sd['_id']) { echo "selected"; } ?> ><?php echo $sd['title']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group mb">
                                    <label class="col-lg-4 control-label">Associated Category</label>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select name="cat" id="cat" class="form-control m-b">
                                                    <option value="">Select Associated Category</option>
                                                    <?php if ($categoriesData) { ?>
                                                        <?php foreach ($categoriesData as $cd) { ?>
                                                            <option value="<?php echo $cd['_id']; ?>" <?php if ($qd['parentCategoryId'] == $cd['_id']) { echo "selected"; } ?>><?php echo $cd['title']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group mb">
                                    <label class="col-lg-4 control-label">Associated Sub-Category</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="subcat" id="subcat" class="form-control m-b">
                                                    <option value="">Select Associated Category</option>
                                                    <?php if ($subCategoriesData) { ?>
                                                        <?php foreach ($subCategoriesData as $scd) { ?>
                                                            <option value="<?php echo $scd['_id']; ?>" <?php if ($qd['categoryId'] == $scd['_id']) { echo "selected"; } ?>><?php echo $scd['title']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group mb">
                                    <input type="hidden" name="id" value="<?php echo $qd['_id']; ?>">
                                    <label class="col-lg-4 control-label">Question</label>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-md-8"><textarea rows="3" name="question" class="form-control"><?php echo $qd['content']; ?></textarea></div>
                                        </div>
                                    </div>
                                </div>                                            
                            <?php } ?>
                        <?php } ?>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?php echo base_url() ?>Questions'">Cancel</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {

        $("#editQues").validate({
            rules: {
                question: "required",
                survey: "required",
                cat: "required"
            },
            messages: {
                question: "Please enter Question",
                survey: "Please select Survey",
                cat: "Please select Category"
            }
        });

    });

</script>