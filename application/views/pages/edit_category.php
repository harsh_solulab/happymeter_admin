<!-- Main section-->
<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Update Category
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>Categories'">Back</button>
        </h3>
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="post" id="editCatFrm" action="<?php echo base_url() ?>Categories/editCat" class="form-horizontal mt-20">
                    <?php if (!empty($catData)) { ?>
                        <?php foreach ($catData as $cd) { ?>
                        <input type="hidden" name="id" value="<?php echo $cd['_id']; ?>">
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Survey</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="survey" id="survey" class="form-control m-b">
                                                <option value="">Select Survey</option>
                                                <?php if ($surveyData) { ?>
                                                    <?php foreach ($surveyData as $sd) { ?>
                                                        <option value="<?php echo $sd['_id']; ?>"  <?php if ($cd['surveyId'] == $sd['_id']) { echo "selected"; } ?>><?php echo $sd['title']; ?></option> <?php } ?> <?php } ?>
                                            </select>
                                        </div>                                
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Category</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="cat" id="cat" value="<?php echo $cd['title']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group mb">
                                <label class="col-lg-4 control-label">Order Id</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="orderid" id="orderid" value="<?php echo $cd['orderId']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-4">                                 
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" onclick="location.href = '<?php echo base_url() ?>/Category'">Cancel</button>
                                </div>
                            </div>
    <?php } ?>
                            <?php } ?>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {
        $("#editCatFrm").validate({
            rules: {
              survey: {
                    required: true                    
                },
                cat: {
                    required: true                    
                },
                orderid: {
                    required: true                    
                }
            }            
        });

    });

</script>