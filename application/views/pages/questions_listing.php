<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Manage Questions
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>dashboard'">Back</button>
        </h3>
        <div class="container-fluid"> 
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="notifications"><?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <?php if ($this->session->flashdata('danger')) { ?>
                <div class="notification"><?php echo $this->session->flashdata('danger'); ?></div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12 mb-20">
                                    <button type="button" class="btn btn-labeled btn-success pull-right" onclick="location.href = '<?php echo base_url() ?>Questions/addQuestions'">
                                        <span class="btn-label"><i class="fa fa-plus"></i>
                                        </span>Add New</button>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="questionManagement" class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Question</th>
                                            <th>Associated Sub-Category</th>
                                            <th>Added on</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($questionData)) {
                                            $count = 1;
                                            ?>
                                            <?php foreach ($questionData as $qd) { ?>                                            
                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td style="width: 40% !important;"><?php echo $qd['content']; ?></td>                                                    
                                                    <td><?php foreach ($subcategoriesData as $sd) { ?><?php
                                                            if ($qd['categoryId'] == $sd['_id']) {
                                                                echo $sd['title'];
                                                            } else {
                                                                echo "";
                                                            }
                                                            ?><?php } ?></td>
                                                    <td><?php
                                                        if (isset($qd['createdAt'])) {
                                                            echo date_format(new DateTime($qd['createdAt']), "Y/m/d H:i:s");
                                                        } else {
                                                            echo date_format(new DateTime(), "Y/m/d H:i:s");
                                                        }
                                                        ?></td>
                                                    <td><a href="<?php echo base_url() ?>Questions/edit_question/<?php echo $qd['_id'] ?>" class="btn btn-primary btn-xs">Edit</a><a href="javascript:;" data-id="<?php echo $qd['_id'] ?>" class="delQues btn btn-danger btn-xs">Delete</a></td>
                                                </tr>  
                                                <?php
                                                $count++;
                                            }
                                            ?>                                        
                                        <?php } else { ?>
                                            <tr>No Data Found</tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(function () {

        /*-----------------User Management-----------------*/
        var UserManagement = $('#questionManagement').dataTable({
            'paging': true, // Table pagination
            'ordering': true, // Column ordering
            'info': true, // Bottom left status text
            'responsive': true, // https://datatables.net/extensions/responsive/examples/
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search Question:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },

        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
                .keyup(function () {
                    UserManagement.fnFilter(this.value, columnInputs.index(this));
                });

        $(document.body).on('click','.delQues',function () {
            var qid = $(this).attr('data-id');

            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>Questions/delete_question',
                data: {qid: qid},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data) {
                        document.location.reload();
                        return false;
                    } else {
                        document.location.reload();
                        return false;
                    }
                }
            });

        });

    });

</script>