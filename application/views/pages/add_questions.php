<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>

            Add New Question
            <small class="sub-title"></small>

            <button type="button" class="btn btn-primary pull-right back-btn" onclick="location.href = '<?php echo base_url() ?>Questions'">Back</button>
        </h3>
        <div class="panel panel-default">
            <div class="panel-body">
                <form method="POST" id="addQues" action="<?php echo base_url() ?>Questions/saveQuestion" class="form-horizontal mt-20">
                    <fieldset>
                         <div class="form-group mb">
                            <label class="col-lg-4 control-label">Associated Survey</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="survey" id="survey" class="form-control m-b">
                                            <option value="">Select Associated Survey</option>
                                            <?php if ($surveyData) { ?>
                                                <?php foreach ($surveyData as $sd) { ?>
                                                    <option value="<?php echo $sd['_id']; ?>"><?php echo $sd['title']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group mb">
                            <label class="col-lg-4 control-label">Associated Category</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="cat" id="cat" class="form-control m-b">
                                            <option value="">Select Associated Category</option>
                                            <?php if ($categoriesData) { ?>
                                                <?php foreach ($categoriesData as $cd) { ?>
                                                    <option value="<?php echo $cd['_id']; ?>"><?php echo $cd['title']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group mb">
                            <label class="col-lg-4 control-label">Associated Sub-Category</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="subcat" id="subcat" class="form-control m-b">
                                            <option value="">Select Associated Sub-Category</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group mb">
                            <label class="col-lg-4 control-label">Question</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-md-8"><textarea rows="3" name="question" class="form-control"></textarea></div>
                                </div>
                            </div>
                        </div>                                            
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" onclick="location.href = '<?php echo base_url() ?>Questions'">Cancel</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(function () {

        $('#cat').on("change", function () {
            var id = $(this).val();
            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>questions/getSubCat',
                data: {id: id},
                dataType: 'html',
                //context: this,
                success: function (data) {
                    if (data) {
                        data = JSON.parse(data);
                        var opt = "";
                        opt = '<option value="">Select Associated Sub-Category</option>';
                                $.each(data, function (index, optiondata) {                                    
                                    opt += "<option value='" + optiondata._id.$id + "'>" + optiondata.title + "</option>";
                                });
                        console.log(opt);
                        $("#subcat").html(opt);
                    } else {
                        //do nothing
                    }
                }
            });

        });
        
        
        	$("#addQues").validate({
			rules: {
				survey: "required",
				cat: "required",
				//subcat: "required",
				question: "required",
			},
			messages: {
				survey: "Please select survey",
				cat: "Please select Category",				
				//subcat: "Please select Sub-Category",				
				question: "Please enter Question",				
			}
		});
        
    });

</script>