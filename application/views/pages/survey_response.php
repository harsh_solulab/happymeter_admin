<section>
    <!-- Page content-->
    <div class="content-wrapper">
        <h3>
            Survey Details
            <button type="button" class="btn btn-primary pull-right back-btn no-margin" onclick="location.href = '<?php echo base_url() ?>Survey'">Back</button>
        </h3>
        <div class="container-fluid">
            <!-- START DATATABLE 1-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- User Details -->
                    <div class="well clearfix survey-questions">
                        <?php
                        if (!empty($resData)) {
                            if (!empty($questionData)) {
                                if (!empty($categoriesData)) {
                                    if (!empty($subCategoriesData)) {
                                        ?>
                                        <?php foreach ($resData as $rd) { ?>
                                            <ul>
                                                <li><?php
                                                    foreach ($categoriesData as $cd) {
                                                        if (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == "") {
                                                            if ($rd['categoryId'] == $cd['_id']) {
                                                                echo $cd['title'];
                                                            }
                                                        } elseif (isset($rd['parentCategoryId']) && $rd['parentCategoryId'] == $cd['_id']) {
                                                            echo $cd['title'];
                                                        }
                                                    }
                                                    ?>
                                                    <ul>
                                                        <li><?php
                                                            foreach ($subCategoriesData as  $scd) {
                                                                if ($rd['categoryId'] == $scd['_id']) {
                                                                    echo $scd['title'];
                                                                }
                                                            }
                                                            ?>
                                                            <ul>
                                                                <li><?php
                                                                    foreach ($questionData as $qd) {
                                                                        if (isset($rd['questionId']) && $rd['questionId'] == $qd['_id']) {
                                                                            echo $qd['content'];
                                                                        }
                                                                    }
                                                                    ?>                                                                    
                                                                </li>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <span class="rangeslider">
                                                                            <input type="text" class="range" value="<?php echo isset($rd['satisfactionLevel']) ? $rd['satisfactionLevel'] : 0; ?>" name="range" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <img src="<?php echo base_url() ?>assets/img/emoji/<?php if (isset($rd['expressions'])) { echo $rd['expressions'] . ".png"; } else { echo "emo0.png"; } ?>" width="35" style="margin-top: 10px">
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <textarea style="width: 100%;" disabled="true"><?php echo isset($rd['comments']) ? $rd['comments'] : "No Comment."; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </ul>
                                                        </li>                                   
                                                    </ul>
                                                </li>                        
                                            </ul>
                                            <?php
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function () {
        $(".range").ionRangeSlider({
            min: 0,
            max: 100,
            disable: true
        });
    });

</script>