<?php

error_reporting(E_ALL);

class Mongo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('mongo_db');
    }

    public function userData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("User");
        $user_data = $select_collection->find();
        if ($data == "info") {
            $userArray = iterator_to_array($user_data);
            return $userArray;
        } else {
            $userArray = iterator_to_array($user_data);
            return COUNT($userArray);
        }
    }

    public function userDataById($id) {

        $select_collection = $this->mongo_db->db->selectCollection("User");
        $id = new MongoId($id);
        $user_data = $select_collection->find(array("_id" => $id));
        $userArray = iterator_to_array($user_data);
        return $userArray;
    }

    public function userSurveyDataById($id, $data) {

        $select_collection = $this->mongo_db->db->selectCollection("UserSurveyMapping");
        //$id = new MongoId($id);
        $user_data = $select_collection->find(array("userId" => $id));
        if ($data == "info") {
            $userArray = iterator_to_array($user_data);
            return $userArray;
        } else {
            $userArray = iterator_to_array($user_data);
            return COUNT($userArray);
        }
    }

    public function surveyData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("Survey");
        $survey_data = $select_collection->find();
        if ($data == "info") {
            $surveyArray = iterator_to_array($survey_data);
            return $surveyArray;
        } else {
            $surveyArray = iterator_to_array($survey_data);
            return COUNT($surveyArray);
        }
    }

    public function invitationData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("Invitation");
        $invitation_data = $select_collection->find();
        if ($data == "info") {
            $inviArray = iterator_to_array($invitation_data);
            return $inviArray;
        } else {
            $inviArray = iterator_to_array($invitation_data);
            return COUNT($inviArray);
        }
    }

    public function categoriesData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $Categories_data = $select_collection->find(array("parentId" => array('$eq' => "")));
        if ($data == "info") {
            $catArray = iterator_to_array($Categories_data);
            return $catArray;
        } else {
            $catArray = iterator_to_array($Categories_data);
            return COUNT($catArray);
        }
    }

    public function subCategoriesData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $Categories_data = $select_collection->find(array("parentId" => array('$ne' => "")));
        if ($data == "info") {
            $catArray = iterator_to_array($Categories_data);
            return $catArray;
        } else {
            $catArray = iterator_to_array($Categories_data);
            return COUNT($catArray);
        }
    }

    public function questionData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("Question");
        $Question_data = $select_collection->find();
        if ($data == "info") {
            $queArray = iterator_to_array($Question_data);
            return $queArray;
        } else {
            $queArray = iterator_to_array($Question_data);
            return COUNT($queArray);
        }
    }

    public function updateUserStatus($user_id, $status) {

        $select_collection = $this->mongo_db->db->selectCollection("User");
        $id = new MongoId($user_id);
        $user_data = $select_collection->update(array('_id' => $id), array('$set' => array('isActive' => $status)));
        if ($user_data) {
            return true;
        } else {
            return false;
        }
    }

    public function userDetails($user_id) {

        $select_collection = $this->mongo_db->db->selectCollection("User");
        $id = new MongoId($user_id);
        $user_data = $select_collection->find(array('_id' => $id));
        return $user_data;
    }

    public function insertInvitationDetails($data, $message) {

        $select_collection = $this->mongo_db->db->selectCollection("Invitation");
        $data = array("email" => $data, "message" => $message, "status" => "Sent", "createdAt" => date(DATE_ISO8601, (new MongoDate())->sec));
        $insert = $select_collection->insert($data);
        if ($insert["ok"]) {
            $newDocID = $data['_id'];
            return $newDocID;
        } else {
            return false;
        }
    }

    public function delInvitation($id) {

        $select_collection = $this->mongo_db->db->selectCollection("Invitation");
        $id = new MongoId($id);
        $delUserFromUserProfile = $select_collection->remove(array('_id' => $id));
        if ($delUserFromUserProfile) {
            return true;
        } else {
            return false;
        }
    }

    public function getSubCat($id) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $Categories_data = $select_collection->find(array("parentId" => $id));
        $catArray = iterator_to_array($Categories_data);
        return $catArray;
    }

    public function saveQuestion($survey, $cat, $subcat, $question) {

        $select_collection = $this->mongo_db->db->selectCollection("Question");
        $data = array("surveyId" => $survey, "parentCategoryId" => $cat, "categoryId" => $subcat, "content" => $question, "createdAt" => date(DATE_ISO8601, (new MongoDate())->sec));
        $insert = $select_collection->insert($data);
        if ($insert["ok"]) {
            return true;
        } else {
            return false;
        }
    }

    public function questionDataById($id) {

        $select_collection = $this->mongo_db->db->selectCollection("Question");
        $id = new MongoId($id);
        $user_data = $select_collection->find(array("_id" => $id));
        $userArray = iterator_to_array($user_data);
        return $userArray;
    }

    public function updQuestion($id,$survey,$cat,$subcat, $question) {

        $select_collection = $this->mongo_db->db->selectCollection("Question");
        $id = new MongoId($id);
        $user_data = $select_collection->update(array('_id' => $id), array('$set' => array("surveyId" => $survey, "parentCategoryId" => $cat, "categoryId" => $subcat ,'content' => $question)));
        if ($user_data) {
            return true;
        } else {
            return false;
        }
    }

    public function userSurveyData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("UserSurveyMapping");
        $user_data = $select_collection->find();
        if ($data == "info") {
            $userArray = iterator_to_array($user_data);
            return $userArray;
        } else {
            $userArray = iterator_to_array($user_data);
            return COUNT($userArray);
        }
    }

    public function surveyResponseData($data) {

        $select_collection = $this->mongo_db->db->selectCollection("SurveyResponse");
        $Question_data = $select_collection->find();
        if ($data == "info") {
            $queArray = iterator_to_array($Question_data);
            return $queArray;
        } else {
            $queArray = iterator_to_array($Question_data);
            return COUNT($queArray);
        }
    }

    public function resData($id) {

        $select_collection = $this->mongo_db->db->selectCollection("SurveyResponse");
        $user_data = $select_collection->find(array("userId" => $id));
        $userArray = iterator_to_array($user_data);
        return $userArray;
    }

    public function delete_question($id) {

        $collection_name = "Question";
        $select_collection = $this->mongo_db->db->selectCollection($collection_name);
        $id = new MongoId($id);
        $delUserFromUserProfile = $select_collection->remove(array('_id' => $id));
        if ($delUserFromUserProfile) {
            return true;
        } else {
            return false;
        }
    }

    public function surveyPCategoryResponseData($data,$user_id) {

        $select_collection = $this->mongo_db->db->selectCollection("SurveyCategoryResponse");
        $scr_data = $select_collection->find(array("parentCategoryId" => array('$eq' => ""),"userId" => $user_id));
        if ($data == "info") {
            $scrArray = iterator_to_array($scr_data);
            return $scrArray;
        } else {
            $scrArray = iterator_to_array($scr_data);
            return COUNT($scrArray);
        }
    }

    public function surveySCategoryResponseData($data,$user_id) {

        $select_collection = $this->mongo_db->db->selectCollection("SurveyCategoryResponse");
        $scr_data = $select_collection->find(array("parentCategoryId" => array('$ne' => ""),"userId" => $user_id));
        if ($data == "info") {
            $scrArray = iterator_to_array($scr_data);
            return $scrArray;
        } else {
            $scrArray = iterator_to_array($scr_data);
            return COUNT($scrArray);
        }
    }

    public function saveCat($survey, $cat, $oid) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $data = array("surveyId" => $survey, "parentId" => "", "orderId" => $oid, "title" => $cat, "description" => $cat);
        $insert = $select_collection->insert($data);
        if ($insert["ok"]) {
            return true;
        } else {
            return false;
        }
    }

    public function catDataById($id) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $id = new MongoId($id);
        $user_data = $select_collection->find(array("_id" => $id));
        $userArray = iterator_to_array($user_data);
        return $userArray;
    }

    public function updCat($id, $survey, $cat, $orderid) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $id = new MongoId($id);
        $user_data = $select_collection->update(array('_id' => $id), array('$set' => array('surveyId' => $survey, "orderId" => $orderid, "title" => $cat, "description" => $cat)));
        if ($user_data) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_cat($id) {

        $collection_name = "Category";
        $select_collection = $this->mongo_db->db->selectCollection($collection_name);
        $id = new MongoId($id);
        $delUserFromUserProfile = $select_collection->remove(array('_id' => $id));
        if ($delUserFromUserProfile) {
            return true;
        } else {
            return false;
        }
    }

    public function saveSubCat($survey, $cat, $subcat) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $data = array("surveyId" => $survey, "parentId" => $cat, "title" => $subcat, "description" => $subcat);
        $insert = $select_collection->insert($data);
        if ($insert["ok"]) {
            return true;
        } else {
            return false;
        }
    }
    
      public function updSubCat($id, $survey, $cat, $subcat) {

        $select_collection = $this->mongo_db->db->selectCollection("Category");
        $id = new MongoId($id);
        $user_data = $select_collection->update(array('_id' => $id), array('$set' => array('surveyId' => $survey, "parentId" => $cat, "title" => $subcat, "description" => $subcat)));
        if ($user_data) {
            return true;
        } else {
            return false;
        }
    }

}

?>