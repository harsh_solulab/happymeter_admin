<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class User extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
         if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['userData'] = $this->mongo_model->userData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/user_listing', $data);
        $this->load->view('templates/footer');
    }

    public function user_details() {

        $user_id = $this->uri->segment(3);
        if ($user_id != "") {
            $data['userData'] = $this->mongo_model->userDataById($user_id);
            $data['userSurveyData'] = $this->mongo_model->userSurveyDataById($user_id, "Count");
        }

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/user_details', $data);
        $this->load->view('templates/footer');
    }

    public function updUserStatus() {
        if ($_POST) {
            $user_id = $_POST['id'];
            $status = $_POST['status'];
            $result = $this->mongo_model->updateUserStatus($user_id, $status);
            if($result){
                $this->session->set_flashdata('success', 'Status changed successfully');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
            }
            echo $result;
        }
    }

}

?>