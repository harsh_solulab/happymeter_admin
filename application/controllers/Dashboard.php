<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
        if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }

        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['userCount'] = $this->mongo_model->userData("Count");
        $data['surveyCount'] = $this->mongo_model->surveyData("Count");
        $data['categoriesCount'] = $this->mongo_model->categoriesData("Count");
        $data['subCategoriesCount'] = $this->mongo_model->subCategoriesData("Count");
        $data['questionCount'] = $this->mongo_model->questionData("Count");
        $data['invitationCount'] = $this->mongo_model->invitationData("Count");



        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/dashboard', $data);
        $this->load->view('templates/footer');
    }

}

?>