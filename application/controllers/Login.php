<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
//        if ($this->session->userdata('logged_in')) {
//            redirect('Dashboard');
//        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {        
        $this->load->view('pages/login');
    }

    public function chklogin() {
        if ($_POST) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            if ($username == "admin" && $password == "admin") {
                $this->load->library('session');
                $newdata = array('logged_in' => TRUE);
                $this->session->set_userdata($newdata);                
                redirect('Dashboard');
            } else {
                $this->session->set_flashdata('danger', 'Invalid username and password');
                redirect('Login');
            }
        }
    }

    public function logout() {
        if ($this->session->userdata('logged_in')) {
            $this->session->unset_userdata('logged_in');
            redirect('Login','refresh');
        }
    }

}

?>