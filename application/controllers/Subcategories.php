<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Subcategories extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
        if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['subcategoriesData'] = $this->mongo_model->subCategoriesData("info");
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/subcategories_listing', $data);
        $this->load->view('templates/footer');
    }

    public function addSubCategory() {
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/add_subcategory', $data);
        $this->load->view('templates/footer');
    }

    public function addSubCat() {
        if ($_POST) {

            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $subcat = $_POST['subcat'];

            $res = $this->mongo_model->saveSubCat($survey, $cat, $subcat);
            if ($res) {
                $this->session->set_flashdata('success', 'Sub Category added succesfully.');
                redirect('Subcategories');
            } else {
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Subcategories');
            }
        }
    }

    public function edit_category() {

        $id = $this->uri->segment(3);
        $data['catData'] = $this->mongo_model->catDataById($id);
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/edit_subcategory', $data);
        $this->load->view('templates/footer');
    }

    public function editSubCat() {
        if ($_POST) {
            $id = $_POST['id'];
            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $subcat = $_POST['subcat'];
            $result = $this->mongo_model->updSubCat($id, $survey, $cat, $subcat);
            if ($result) {
                $this->session->set_flashdata('success', 'Sub Category update successfully');
                redirect('Subcategories');
            } else {
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Subcategories');
            }
        }
    }
    
        public function delete_cat() {
        if ($_POST) {
            $cid = $_POST['cid'];            
            $result = $this->mongo_model->delete_cat($cid);
            if($result){
                $this->session->set_flashdata('success', 'Sub Category deleted successfully');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
            }
            echo $result;
        }
    }

}

?>