<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Invitation extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
         if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['invitationData'] = $this->mongo_model->invitationData("info");

        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/invitation_listing', $data);
        $this->load->view('templates/footer');
    }

    public function addInvitation() {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/sending_invitation');
        $this->load->view('templates/footer');
    }

    public function sendInvitation() {
        if ($_POST) {
            //$message = "<html><body>Hello,<br/>Greetings from Team HappyMeter!!<br/>You have been invited to fill up a quick survey on our platform. Please go to below URL and create an account. Post that you will be able to fill up the survey and submit to us.<br/><a href='" + FRONTURL + "'>Click Here</a><br/>We will share you your results once you submitted.<br/>Regards,<br/>Team HappyMeter</body></html>";
            $message = "Hello,<br/>"
                    . "Greetings from Team HappyMeter!!<br/>"
                    . "You have been invited to fill up a quick survey on our platform. Please go to below URL and create an account. Post that you will be able to fill up the survey and submit to us.<br/>"
                    . "<a href='" . FRONTURL . "' target='_blank'>Click Here</a><br/>"
                    . "We will share you your results once you submitted.<br/>"
                    . "Regards,<br/>"
                    . "Team HappyMeter";
            $res = $this->send_email($_POST['email'], "Invitation", $message);

            if ($res) {
                $ins = $this->mongo_model->insertInvitationDetails($_POST['email'], $message);
                if ($ins) {
                    $this->session->set_flashdata('success', 'Invitation sent succesfully.');
                    redirect('invitation');
                } else {
                    $this->session->set_flashdata('danger', 'Data not inserted.');
                    redirect('invitation');
                }
            } else {
                $this->session->set_flashdata('danger', 'Invitation not sent.');
                redirect('invitation');
            }
        }
    }

    public function send_email($to, $subject, $message) {

        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'happymeterdevs@gmail.com';
        $config['smtp_pass'] = 'Happy@123*';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      
        $this->email->initialize($config);

        $this->email->from('happymeterdevs@gmail.com', 'Happy Meter');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if (!$this->email->send()) {
//echo $this->email->print_debugger();
            return false;
        }
//echo "Mail sent";
        return true;
    }

    public function resendInvitation() {
        if ($_POST) {
            $id = $_POST['email'];

            $message = "Hello,<br/>"
                    . "Greetings from Team HappyMeter!!<br/>"
                    . "You have been invited to fill up a quick survey on our platform. Please go to below URL and create an account. Post that you will be able to fill up the survey and submit to us.<br/>"
                    . "<a href='" . FRONTURL . "' target='_blank'>Click Here</a><br/>"
                    . "We will share you your results once you submitted.<br/>"
                    . "Regards,<br/>"
                    . "Team HappyMeter";
            $res = $this->send_email($_POST['email'], "Invitation", $message);

            if ($res) {
                $ins = $this->mongo_model->insertInvitationDetails($_POST['email'], $message);
                if ($ins) {
                    $this->session->set_flashdata('success', 'Invitation sent succesfully.');
                    echo true;
                } else {
                    $this->session->set_flashdata('danger', 'Data not inserted.');
                    echo false;
                }
            } else {
                $this->session->set_flashdata('danger', 'Invitation not sent.');
                echo false;
            }
        }
    }

    public function delInvitation() {
        if ($_POST) {
            $id = $_POST['id'];

            $del = $this->mongo_model->delInvitation($id);
            if ($del) {
                $this->session->set_flashdata('success', 'Invitation deleted succesfully.');
                echo true;
            } else {
                $this->session->set_flashdata('danger', 'Invitation not deleted.');
                echo false;
            }
        }
    }

}

?>