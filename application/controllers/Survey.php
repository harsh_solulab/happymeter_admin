<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Survey extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
         if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['userData'] = $this->mongo_model->userData("info");
        $data['userSurveyData'] = $this->mongo_model->userSurveyData("info");
        $data['questionData'] = $this->mongo_model->questionData("Count");
        $data['surveyResponseData'] = $this->mongo_model->surveyResponseData("info");
                
        
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/survey_listing', $data);
        $this->load->view('templates/footer');
    }
    
    public function view_answer() {
        
        $user_id = $this->uri->segment(3);
        
        $data['resData'] = $this->mongo_model->resData($user_id);
        $data['questionData'] = $this->mongo_model->questionData("info");
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['subCategoriesData'] = $this->mongo_model->subCategoriesData("info");
        $data['surveyResponseData'] = $this->mongo_model->surveyResponseData("info");                
                        
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/survey_response', $data);
        $this->load->view('templates/footer');
    }
    
    public function view_report() {
        
        $user_id = $this->uri->segment(3);
        
        $data['resData'] = $this->mongo_model->resData($user_id);
        $data['questionData'] = $this->mongo_model->questionData("info");
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['subCategoriesData'] = $this->mongo_model->subCategoriesData("info");
        $data['surveyResponseData'] = $this->mongo_model->surveyResponseData("info");                
        $data['surveyPCategoryResponseData'] = $this->mongo_model->surveyPCategoryResponseData("info",$user_id);
        $data['surveySCategoryResponseData'] = $this->mongo_model->surveySCategoryResponseData("info",$user_id);
                        
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/survey_report', $data);
        $this->load->view('templates/footer');
    }

}

?>