<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
        if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/categories_listing', $data);
        $this->load->view('templates/footer');
    }

    public function addCategory() {
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/add_category', $data);
        $this->load->view('templates/footer');
    }

    public function addCat() {
        if ($_POST) {

            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $orderid = $_POST['orderid'];

            $res = $this->mongo_model->saveCat($survey, $cat, $orderid);
            if ($res) {
                $this->session->set_flashdata('success', 'Category added succesfully.');
                redirect('Categories');
            } else {
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Categories');
            }
        }
    }
    
    public function edit_category() {

        $id = $this->uri->segment(3);        
        $data['catData'] = $this->mongo_model->catDataById($id);
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/edit_category', $data);
        $this->load->view('templates/footer');
    }
    
      public function editCat() {
        if ($_POST) {
            $id = $_POST['id'];
            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $orderid = $_POST['orderid'];            
            $result = $this->mongo_model->updCat($id, $survey,$cat,$orderid);
            if($result){
                $this->session->set_flashdata('success', 'Category update successfully');
                redirect('Categories');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Categories');
            }            
        }
    }
    
      public function delete_cat() {
        if ($_POST) {
            $cid = $_POST['cid'];            
            $result = $this->mongo_model->delete_cat($cid);
            if($result){
                $this->session->set_flashdata('success', 'Category deleted successfully');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
            }
            echo $result;
        }
    }

}

?>