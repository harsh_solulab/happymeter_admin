<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

error_reporting(E_ALL);

class Questions extends CI_Controller {

    public function __construct() {
        parent::__construct(); // you have missed this line.
         if (!$this->session->userdata('logged_in')) {
            redirect('Login');
        }
        $this->load->library('mongo_db');
        $this->load->model('mongo_model');
    }

    public function index() {

        $data['questionData'] = $this->mongo_model->questionData("info");
        $data['subcategoriesData'] = $this->mongo_model->subCategoriesData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/questions_listing', $data);
        $this->load->view('templates/footer');
    }

    public function addQuestions() {

        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/add_questions.php', $data);
        $this->load->view('templates/footer');
    }

    public function getSubCat() {
        if ($_POST) {
            $id = $_POST['id'];
            $res = $this->mongo_model->getSubCat($id);
            echo json_encode($res);
        }
    }

    public function saveQuestion() {
        if ($_POST) {

            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $subcat = $_POST['subcat'];
            $question = $_POST['question'];

            $res = $this->mongo_model->saveQuestion($survey, $cat, $subcat, $question);
            if ($res) {
                $this->session->set_flashdata('success', 'Question added succesfully.');
                redirect('Questions');
            } else {
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Questions');
            }
        }
    }

    public function edit_question() {

        $id = $this->uri->segment(3);
        $data['categoriesData'] = $this->mongo_model->categoriesData("info");
        $data['subCategoriesData'] = $this->mongo_model->subCategoriesData("info");
        $data['surveyData'] = $this->mongo_model->surveyData("info");
        $data['quesData'] = $this->mongo_model->questionDataById($id);
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('pages/edit_questions', $data);
        $this->load->view('templates/footer');
    }
    
       public function updQuestion() {
        if ($_POST) {
            $id = $_POST['id'];
            $survey = $_POST['survey'];
            $cat = $_POST['cat'];
            $subcat = $_POST['subcat'];
            $question = $_POST['question'];
            $result = $this->mongo_model->updQuestion($id,$survey,$cat,$subcat, $question);
            if($result){
                $this->session->set_flashdata('success', 'Question update successfully');
                redirect('Questions');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
                redirect('Questions');
            }            
        }
    }
    
       public function delete_question() {
        if ($_POST) {
            $qid = $_POST['qid'];            
            $result = $this->mongo_model->delete_question($qid);
            if($result){
                $this->session->set_flashdata('success', 'Question deleted successfully');
            }else{
                $this->session->set_flashdata('danger', 'Something went wrong.');
            }
            echo $result;
        }
    }

}

?>