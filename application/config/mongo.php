<?php
/*
MongoDB config file
@since: 1st Nov 2014
*/

//MongoDB connection detail for local

//mongodb host 
$config['default']['mongo_hostbase'] = 'localhost';
//mongodb name
$config['default']['mongo_database'] = 'happymeter-dev1';
//mongodb username - by default, it is empty
$config['default']['mongo_username'] = '';
//mongodb password - by default, it is empty
$config['default']['mongo_password'] = '';

?>
